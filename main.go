package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"
)

type myHandler struct {
}

func (t *myHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	_path := r.URL.Path
	fmt.Println("req path ", _path)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(200)
	switch _path {
	case "/api/v1/list":
		json.NewEncoder(w).Encode(map[string]interface{}{
			"status": "ok",
			"objs": []interface{}{
				time.Now().Format("2006-01-02 15:04:05"),
				"只是测试而已",
				"这个是个测试列表",
			},
		})
	default:
		json.NewEncoder(w).Encode(map[string]interface{}{
			"status":  "error",
			"message": fmt.Sprintf("不支持的api:%v", _path),
		})
	}
}

func main() {
	port := os.Getenv("SERVER_PORT")
	if len(port) == 0 {
		port = "8100"
	}
	server := http.Server{
		Addr:    fmt.Sprintf(":%v", port),
		Handler: &myHandler{},
	}
	fmt.Println("启动端口:", port)
	server.ListenAndServe()
}
